import _ from 'lodash'
import { saveAs } from 'file-saver'
import { backend } from '~/library/main'
import helpers from '~/library/helpers'

const VuexPluginLocalStorage = (store) => {
  let dataStored = localStorage.getItem('proton-state')
  if (dataStored) {
    dataStored = JSON.parse(dataStored)
  }
  if (
    dataStored &&
    dataStored.VERSION &&
    dataStored.VERSION === LOCAL_STORAGE_DATA_VERSION
  ) {
    store.commit('__load__', dataStored)
  } else {
    localStorage.removeItem('proton-state')
  }

  store.subscribe((mutation, state) => {
    localStorage.setItem('proton-state', JSON.stringify(state))
  })
}

export const plugins = [VuexPluginLocalStorage]

const LOCAL_STORAGE_DATA_VERSION = 1
export const state = () => ({
  VERSION: LOCAL_STORAGE_DATA_VERSION,

  // Admin states
  isLoggedIn: false,
  loggedInToken: false,
  loggedInRole: null,
  loggedInUser: null,

  // Selected participant
  activeParticipant: null,

  // Bookable events for reg admin
  bookableEvents: [],

  // Item selected by reg admin for reg
  selectedBookableEvent: null,

  // All events for data admin
  allEvents: [],
  selectedEvent: null,
  filteredParticipants: []
})

export const mutations = {
  __load__(state, stateNew) {
    const stateNewFiltered = _.pick(stateNew, [
      'isLoggedIn',
      'loggedInToken',
      'loggedInRole',
      'loggedInUser'
    ])
    Object.assign(state, stateNewFiltered)
  },
  setLoggedIn(state, payload) {
    state.loggedInToken = payload.token
    state.isLoggedIn = true
    state.loggedInRole = payload.role
    state.loggedInUser = payload.username
  },
  setLoggedOut(state) {
    state.loggedInToken = null
    state.isLoggedIn = false
    state.loggedInRole = null
    state.loggedInUser = null
  },

  setActiveParticipant(state, participant) {
    state.activeParticipant = participant
  },
  unsetActiveParticipant(state) {
    state.activeParticipant = null
  },
  setBookableEvents(state, events) {
    state.bookableEvents = events
  },
  setSelectedBookableEvent(state, event) {
    state.selectedBookableEvent = event
  },
  setAllEvents(state, events) {
    state.allEvents = events
  },
  setSelectedEvent(state, event) {
    state.selectedEvent = event
  },
  // Only used in email admin page
  setFilteredParticipants(state, largeList) {
    state.filteredParticipants = largeList
  }
}

export const actions = {
  async login(context, loginData) {
    try {
      const resp = await backend.login(loginData)
      context.commit('setLoggedIn', {
        username: resp.username,
        token: resp.token,
        role: resp.role
      })
    } catch (e) {
      throw e
    }
  },
  logout(context) {
    context.commit('setLoggedOut')
  },

  async findParticipant(context, short_id) {
    let resp = null
    try {
      resp = await backend.findParticipant(
        context.state.loggedInToken,
        short_id
      )
      if (resp == null) throw new Error('NO_USER')
      context.commit('setActiveParticipant', resp)
      return resp
    } catch (e) {
      throw e
    }
  },

  async issueCommonPassToActiveParticipant(context) {
    await backend.issuePass(
      context.state.loggedInToken,
      context.state.activeParticipant._id
    )
  },

  async refreshActiveParticipant(context) {
    const resp = await backend.findParticipant(
      context.state.loggedInToken,
      context.state.activeParticipant.short_id
    )
    context.commit('setActiveParticipant', resp)
  },

  async loadBookableEvents(context) {
    const resp = await backend.getEvents()
    const bookableEvents = resp.filter((event) => {
      const topcategory = event.category.split(':')[0]
      return topcategory === 'workshop' || topcategory === 'lecture'
    })
    context.commit('setBookableEvents', bookableEvents)
  },

  async loadEvents(context) {
    const resp = await backend.getEvents()
    context.commit('setAllEvents', resp)
  },

  async selectItem(context, item_id) {
    const item = await backend.getEventAdmin(
      context.state.loggedInToken,
      item_id
    )
    console.log(item)
    context.commit('setSelectedBookableEvent', item)
  },
  unselectItem(context) {
    context.commit('setSelectedBookableEvent', null)
  },

  async refreshSelectedBookableEvent(context) {
    const item = await backend.getEventAdmin(
      context.state.loggedInToken,
      context.state.selectedBookableEvent._id
    )
    context.commit('setSelectedBookableEvent', item)
  },

  async returnAdminsList(context) {
    const list = await backend.getAllAdmins(context.state.loggedInToken)
    return list
  },

  async changeEmail(context, pdata) {
    await backend.changeEmail(
      context.state.loggedInToken,
      pdata.authID,
      pdata.email
    )
  },
  async correctParticipantProfile(context, profile) {
    await backend.adminChangeParticipantProfile(
      context.state.loggedInToken,
      context.state.activeParticipant._id,
      profile
    )
  },
  async fetchFilteredParticipants(context, filters) {
    const filteredParticipants = await backend.getFilteredParticipants(
      context.state.loggedInToken,
      filters
    )
    context.commit('setFilteredParticipants', filteredParticipants)
  },

  async adminRegParticipant(context) {
    console.log('Selected event', context.state.selectedBookableEvent._id)
    await backend.adminRegEvent(
      context.state.loggedInToken,
      context.state.activeParticipant._id,
      context.state.selectedBookableEvent._id
    )
  },

  async unregEventOfParticipant(context, event_id) {
    await backend.adminStripOffEvent(
      context.state.loggedInToken,
      context.state.activeParticipant._id,
      event_id
    )
  },

  async giveAttendance(context) {
    await backend.markAttendanceForEvent(
      context.state.loggedInToken,
      context.state.selectedBookableEvent._id,
      context.state.activeParticipant.short_id
    )
  },

  async editEvent(context, edits) {
    await backend.editEvent(
      context.state.loggedInToken,
      context.state.selectedBookableEvent._id,
      edits
    )
  },

  verifyOfflineSignedQR(context, data) {
    return helpers.verifyOfflineSignedQR(data)
  },

  async addAdmin(context, admin) {
    await backend.addAdmin(context.state.loggedInToken, admin)
  },

  async getCertificateQR(context, body) {
    const blob = await backend.getCertificateQR(
      context.state.loggedInToken,
      body
    )
    saveAs(blob, body.short_id + '.png')
  }

  // Backend API has not been implemented for the following functions. Directly edit database instead!
  // fetchAdminsList(context) {
  //   return backend.getAdminsList(context.state.loggedInToken)
  // },

  // async removeAdmin(context, admin_id) {
  //   await backend.removeAdmin(context.state.loggedInToken, admin_id)
  // }
}
