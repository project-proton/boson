export default function({ store, route, redirect }) {
  if (
    route.meta[0] &&
    ((route.meta[0].routeOnlyWhen === 'LOGGED_OUT' && store.state.isLoggedIn) ||
      (route.meta[0].routeOnlyWhen === 'LOGGED_IN' && !store.state.isLoggedIn))
  ) {
    redirect('/')
  }
  if (
    route.meta[0].isAllowed &&
    !route.meta[0].isAllowed(store.state.loggedInRole, store.state.isLoggedIn)
  )
    redirect('/')
}
