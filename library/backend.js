/* eslint-disable camelcase */
/* eslint require-await: 0 */
import ky from 'ky'

const API_ROOT = 'https://staging.tathva.org'

async function register({
  name,
  gender,
  college,
  email,
  mobile,
  userid,
  referral_code,
  password
}) {
  const participant = {
    name,
    gender,
    college,
    email,
    mobile,
    username: userid,
    password,
    referral_code
  }
  try {
    await ky.post(API_ROOT + '/auth/participant/register', {
      json: participant
    })
    return { status: 'OK' }
  } catch (e) {
    if (!e.response) throw e
    switch (e.response.status) {
      case 400:
        return {
          status: 'ERR',
          code: 'E_BAD_REQUEST',
          body: await e.response.text()
        }
      case 500:
        return { status: 'ERR', code: 'E_USER_EXISTS' }
      default:
        throw e
    }
  }
}

async function getCertificateQR(token, body) {
  const url = API_ROOT + '/events/' + body.event_id + '/certificate'
  let res = await ky.post(url, {
    headers: { Authorization: 'Bearer ' + token },
    json: { short_id: body.short_id, type: body.type }
  })
  res = await res.blob()
  return res
}
async function addAdmin(token, admin) {
  const url = API_ROOT + '/auth/admin/register'
  const { username, password, role } = admin
  await ky.post(url, {
    headers: { Authorization: 'Bearer ' + token },
    json: {
      username,
      password,
      role
    }
  })
}

async function login({ userid, password }) {
  let res = await ky.post(API_ROOT + '/auth/admin/get-token', {
    json: { username: userid, password }
  })
  res = await res.json()
  if (res.username === 'tathva19') res.role = 'data'
  return res
}

async function issuePass(token, authID) {
  const url = API_ROOT + '/participants/' + authID + '/issuepass'
  await ky.get(url, {
    headers: { Authorization: 'Bearer ' + token }
  })
}

async function adminRegEvent(token, authID, event_id) {
  const url = API_ROOT + '/participants/' + authID + '/pushevent'
  await ky.post(url, {
    headers: { Authorization: 'Bearer ' + token },
    json: { event_id }
  })
}

async function adminStripOffEvent(token, authID, event_id) {
  const url = API_ROOT + '/participants/' + authID + '/pullevent'
  await ky.post(url, {
    headers: { Authorization: 'Bearer ' + token },
    json: { event_id }
  })
}

async function markAttendanceForEvent(token, event_id, short_id) {
  const url = API_ROOT + '/events/' + event_id + '/attendance'
  await ky.post(url, {
    headers: { Authorization: 'Bearer ' + token },
    json: { short_id }
  })
}

async function editEvent(token, event_id, edits) {
  const url = API_ROOT + '/events/' + event_id
  const { name, description, meta } = edits
  await ky.put(url, {
    headers: { Authorization: 'Bearer ' + token },
    json: {
      name,
      description,
      meta
    }
  })
}

async function findParticipant(token, short_id) {
  const url = API_ROOT + '/participants/' + short_id
  let resp = await ky.get(url, {
    headers: { Authorization: 'Bearer ' + token }
  })
  resp = (await resp.json()).participant
  return resp
}

async function changeEmail(token, auth_id, newemail) {
  const url = API_ROOT + '/participants/' + auth_id
  await ky.put(url, {
    headers: { Authorization: 'Bearer ' + token },
    json: { email: newemail }
  })
}

async function adminChangeParticipantProfile(token, p, profile) {
  const url = API_ROOT + '/participants/' + p
  const { name, college, gender, mobile } = profile
  await ky.put(url, {
    headers: { Authorization: 'Bearer ' + token },
    json: {
      name,
      college,
      gender,
      mobile
    }
  })
}

async function getFilteredParticipants(token, filters) {
  const url = API_ROOT + '/participants/slim'
  const { name, email, mobile, authID, college } = filters
  const resp = await ky.post(url, {
    headers: { Authorization: 'Bearer ' + token },
    json: {
      name,
      email,
      mobile,
      authID,
      college
    }
  })
  const participants = (await resp.json()).participants
  return participants
}

async function getAllAdmins(token) {
  const url = API_ROOT + '/admins'
  let resp = await ky.get(url, {
    headers: { Authorization: 'Bearer ' + token }
  })
  resp = resp.json().admins
  return resp
}

async function getEvents() {
  try {
    const res = await ky.get(API_ROOT + '/events')
    const events = await res.json()
    return events
  } catch (e) {
    throw e
  }
}

async function getEventAdmin(token, event_id) {
  try {
    const url = API_ROOT + '/events/' + event_id
    const res = await ky.get(url, {
      headers: { Authorization: 'Bearer ' + token }
    })
    const event = await res.json()
    return event
  } catch (e) {
    throw e
  }
}

async function getInterestedItems(token) {
  const url = API_ROOT + '/participants/self/interested'
  const res = await ky.get(url, {
    headers: { Authorization: 'Bearer ' + token }
  })
  return (await res.json()).interested
}
async function getBookedItems(token) {
  const url = API_ROOT + '/participants/self/events'
  const res = await ky.get(url, {
    headers: { Authorization: 'Bearer ' + token }
  })
  return (await res.json()).events
}

async function updateParticipantProfile(token, patches) {
  try {
    const res = await ky.put(API_ROOT + '/participants/self', {
      headers: { Authorization: 'Bearer ' + token },
      json: patches
    })
    if (res.statusCode !== 200);
  } catch (e) {
    throw e
  }
}

async function registerEvents(token, eventIds) {
  try {
    const res = await ky.post(API_ROOT + '/payments/checkout', {
      headers: { Authorization: 'Bearer ' + token },
      json: { events: eventIds }
    })
    const body = await res.json()
    return {
      status: 'OK',
      redirect_url: body.payment_request_url
    }
  } catch (e) {
    if (!e.response) throw e
    switch (e.response.status) {
      case 401:
        return { status: 'ERR', code: 'E_TOKEN_EXPIRED' }
      case 422:
        return { status: 'ERR', code: 'E_NO_STOCK' }
      case 500:
        return { status: 'ERR', code: 'E_THIRD_PARTY' }
      case 503:
        return { status: 'ERR', code: 'E_HOURLY_LIMIT' }
      case 505:
        return { status: 'ERR', code: 'E_TEMP_MAINT' }
      default:
        throw e
    }
  }
}

export default {
  register,
  login,
  getEvents,
  registerEvents,
  updateParticipantProfile,
  getInterestedItems,
  getBookedItems,
  findParticipant,
  getEventAdmin,
  getFilteredParticipants,
  getAllAdmins,
  changeEmail,
  issuePass,
  adminRegEvent,
  adminStripOffEvent,
  markAttendanceForEvent,
  adminChangeParticipantProfile,
  editEvent,
  addAdmin,
  getCertificateQR
}
