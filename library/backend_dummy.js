/* eslint require-await: 0 */

import _ from 'lodash'

const TOKEN = 'f3a9014c'

const DATA = {
  participant: null,
  participantRegisteredEvents: []
}

function _loadFromLocalStorage() {
  const dataStored = localStorage.getItem('proton-backend-dummy')
  if (dataStored) {
    Object.assign(DATA, JSON.parse(dataStored))
  }
}
_loadFromLocalStorage()

function _saveToLocalStorage() {
  localStorage.setItem('proton-backend-dummy', JSON.stringify(DATA))
}

const EVENTS = [
  {
    _id: '53403a25-15bd-4f7d-aa9d-f0b6641f76f4',
    name: 'deposit turquoise Fresh',
    description: 'Internal Vatu Refined Wooden Bike',
    category: 'Workshop',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 87
  },
  {
    _id: '10b8c239-0a43-4bbb-91db-7bdb9a064655',
    name: 'Burundi Franc',
    description: 'SDD',
    category: 'Workshop',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 52
  },
  {
    _id: '7e7cfb01-8486-4264-ada3-344c664b2160',
    name: 'Generic Metal Fish best-of-breed B2B',
    description: 'Games',
    category: 'Workshop',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 88
  },
  {
    _id: '7b91dcf4-e223-4613-a7e2-db42275fd81e',
    name: 'Analyst',
    description: 'Arizona',
    category: 'Workshop',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 92
  },
  {
    _id: '0044a65b-dc15-438b-b3a9-52f68acfdf60',
    name: 'Nepal Auto Loan Account Bike',
    description: 'Metical Azerbaijanian Manat Beauty',
    category: 'Workshop',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 90
  },
  {
    _id: '2f40d44c-a8f3-455a-aea1-f7d7bf166dcf',
    name: 'redundant fuchsia Montana',
    description: 'South Carolina blue',
    category: 'Workshop',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 76
  },
  {
    _id: 'ed723ae2-7675-4880-a4b7-1d39e0075c95',
    name: 'Utah',
    description: 'override Towels Oklahoma',
    category: 'Workshop',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 81
  },
  {
    _id: '57c7474c-a898-4b0a-9b5a-6aba66226eb4',
    name: 'Guam',
    description: 'Connecticut',
    category: 'Workshop',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 65
  },
  {
    _id: '7e992431-dd34-486d-9dac-d130e23a2b12',
    name: 'Buckinghamshire infomediaries maroon',
    description: 'calculating',
    category: 'Workshop',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 86
  },
  {
    _id: 'b64182e0-d381-40ca-860f-6760d36ac710',
    name: 'Canadian Dollar capacity',
    description: 'static',
    category: 'Workshop',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 65
  },
  {
    _id: '8a5cf88c-6a46-4a5e-8782-8350440edd88',
    name: 'Profit-focused secondary Graphical User Interface',
    description:
      'Aut quia qui qui similique fugiat quis quo asperiores voluptatem. Sapiente necessitatibus eaque. Est sit consectetur debitis alias. Quia fugiat omnis. Ipsam eos unde hic incidunt nulla consequatur. Molestiae autem vitae sed libero molestias ad voluptatem aut.',
    category: 'Events/Computer Science',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 79
  },
  {
    _id: '1814b14c-2d0c-4c56-be14-47f9b9fd49da',
    name: 'Triple-buffered bandwidth-monitored software',
    description:
      'Voluptas magnam non laudantium et at possimus est. Non debitis ut veniam molestiae enim quidem perferendis veniam voluptates. Eligendi non animi. Sit consequuntur voluptas. Est molestiae corrupti enim natus perferendis. Voluptate iure consequatur neque.',
    category: 'Events/Computer Science',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 56
  },
  {
    _id: '6bf4428b-6813-4f6a-9b5d-5ce1b593b941',
    name: 'Versatile logistical hierarchy',
    description:
      'Eveniet placeat expedita optio. Et aliquam eum soluta repellat aut. Qui eum optio velit voluptas autem eum illo nisi est. Libero ipsa reprehenderit nisi quidem eveniet accusantium dignissimos. Id fuga vitae voluptates ex illum reprehenderit soluta.',
    category: 'Events/Computer Science',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 69
  },
  {
    _id: 'bfce40dd-22eb-487f-b2bd-cf8a632d3c19',
    name: 'Open-architected intangible encoding',
    description:
      'Debitis non voluptatem qui iusto maxime beatae et. Ab aut aut. Et quis et. Ipsa quos doloremque minima dolorum. Eos eligendi ipsum omnis ut velit sapiente enim ut consequatur.',
    category: 'Events/Computer Science',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 87
  },
  {
    _id: '92ee7607-b6d5-4c33-9007-6d4623a40053',
    name: 'Right-sized context-sensitive hierarchy',
    description:
      'Non eos magni est repellendus reprehenderit expedita tenetur. Rerum qui sint enim et accusamus exercitationem esse sapiente.',
    category: 'Events/Computer Science',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 89
  },
  {
    _id: '48a6e1de-37e9-4d18-a6ea-0b37b3621f59',
    name: 'Balanced systematic access',
    description:
      'Nihil repellendus assumenda enim. Et est est minima magnam non ut. Quis rerum recusandae. Eligendi ut quas eius similique nobis mollitia odit nobis. Et labore optio quas.',
    category: 'Events/Computer Science',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 80
  },
  {
    _id: 'f2bcf6f9-d67c-4d79-b5b2-3fa183a6733c',
    name: 'Persistent bifurcated throughput',
    description:
      'Culpa amet debitis ab ab qui voluptas eum. Est earum explicabo totam aperiam. Nobis possimus perferendis aut voluptas deleniti ut ut ullam assumenda. Est porro adipisci commodi perferendis enim laborum aliquid qui accusamus. Iure eius nam voluptate veritatis magni. Autem et in quasi maiores eos nostrum iste.',
    category: 'Events/Computer Science',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 64
  },
  {
    _id: '1fabfe35-eef5-44ac-859e-eaf88d4af66f',
    name: 'Quality-focused motivating projection',
    description:
      'Architecto et laborum voluptatem facilis ratione. Voluptatum aspernatur est hic provident vel dignissimos.',
    category: 'Events/Computer Science',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 99
  },
  {
    _id: 'f657038d-1746-4d8a-a318-b18c6fbe5fbf',
    name: 'Organic systematic strategy',
    description:
      'Deserunt dignissimos enim occaecati libero ducimus adipisci quisquam sunt est. Magni libero rem vero fuga sint maxime illum nobis aut. Iste et dicta qui nesciunt assumenda. Natus aut et doloribus. Repudiandae dolores ullam officiis autem ad et ut. Vero cupiditate nihil distinctio.',
    category: 'Events/Computer Science',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 92
  },
  {
    _id: '6edb1c32-8d47-4466-958e-40b27146022d',
    name: 'Intuitive systemic initiative',
    description:
      'Impedit labore perspiciatis id deleniti nobis explicabo temporibus. Ex corrupti molestiae et esse aut. Ducimus labore ducimus minus. Molestiae et fugiat voluptatem. Natus quod quo et et aut nemo.',
    category: 'Events/Computer Science',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 81
  },
  {
    _id: '77e779a3-2803-4978-be3e-b46b3c060177',
    name: 'Exclusive responsive conglomeration',
    description:
      'Quasi aut voluptatum voluptas sit. Aut et ducimus itaque. Dolores et consequatur quia nesciunt et totam aut. Et dolore et temporibus odio iste soluta qui dolores deserunt. Est perferendis molestiae eius fugiat rerum tempore libero est dignissimos.',
    category: 'Events/Mechanical',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 54
  },
  {
    _id: 'db0d3840-f5d8-4088-b7df-ecc9496f6998',
    name: 'Multi-lateral explicit migration',
    description:
      'Ea aut impedit voluptatibus quisquam. Cupiditate et eligendi earum minima dolores harum occaecati molestias maxime. Corporis maiores voluptas. Aliquam aut adipisci nihil et et mollitia quidem officia. Provident architecto saepe rerum unde voluptatum.',
    category: 'Events/Mechanical',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 98
  },
  {
    _id: '62cc21f4-53c8-4a12-bb9e-e8cbebe7d50b',
    name: 'Reactive object-oriented service-desk',
    description:
      'Tempora hic vitae nihil perspiciatis. Rerum quia necessitatibus esse tempora illum eos minima. Adipisci eaque vel sint. Optio voluptate cupiditate in. Quibusdam vero incidunt dolorem voluptatum iste. Aliquid aut voluptatum perferendis doloribus dolores velit rem molestias error.',
    category: 'Events/Mechanical',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 58
  },
  {
    _id: 'ab984a55-10ef-431c-86f4-e74b9953af5a',
    name: 'Digitized uniform function',
    description:
      'Id maxime ut ipsa error impedit unde rerum tempore. Ipsum et quo ut dolorum. Excepturi harum iusto perferendis.',
    category: 'Events/Mechanical',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 74
  },
  {
    _id: '2a5a08e6-4586-475b-9e28-287d069bdc1c',
    name: 'Universal contextually-based paradigm',
    description:
      'Qui facere necessitatibus. Expedita inventore sit est optio doloremque in. Voluptates itaque repellendus consequuntur sequi rem amet. Suscipit est inventore rerum.',
    category: 'Events/Mechanical',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 76
  },
  {
    _id: '403a6ffb-3b6b-465b-adb9-dc952fb624c3',
    name: 'Optimized 6th generation initiative',
    description:
      'Laborum ut dicta. Dolore eum nihil harum et laborum porro. Sint voluptatibus incidunt ipsa rem. Similique aliquid qui nihil facere sit. Suscipit rerum voluptatem vero voluptas.',
    category: 'Events/Mechanical',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 82
  },
  {
    _id: '8c9cd0c2-c009-4c7b-af4c-4ad00d009453',
    name: 'Ergonomic empowering secured line',
    description:
      'Quis exercitationem odit quisquam eius. Dolorem quia nesciunt in repudiandae asperiores dignissimos impedit sed. Et delectus qui non molestiae modi nam explicabo consequuntur. Modi ut occaecati consequatur quasi quis iste molestiae quia aut.',
    category: 'Events/Mechanical',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 55
  },
  {
    _id: '99ed2792-20b6-4cb6-bd9c-716e5fc9842d',
    name: 'Managed stable budgetary management',
    description:
      'Repellendus nulla omnis voluptate aliquid. Non possimus perferendis inventore deleniti qui qui. Rerum autem et id voluptatum eius sint.',
    category: 'Events/Mechanical',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 95
  },
  {
    _id: '16bd61ee-5d3a-48ed-b328-c9308fcc307e',
    name: 'Visionary radical archive',
    description: 'Adipisci nobis officia dolor dolorem fugiat. Et omnis non.',
    category: 'Events/Mechanical',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 63
  },
  {
    _id: '874f8acb-9eba-4a80-b30c-8b8833b32949',
    name: 'Cross-group didactic local area network',
    description:
      'Nihil quibusdam magni. Voluptatem qui dolorem. Id recusandae error incidunt minus est id eligendi sapiente.',
    category: 'Events/Mechanical',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 65
  },
  {
    _id: '92b70988-511a-474c-893c-206c06afdf79',
    name: 'Extended multimedia core',
    description:
      'Sint illo voluptates cupiditate. Enim assumenda similique aliquam pariatur.',
    category: 'Events/General',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 52
  },
  {
    _id: 'd46104c3-0399-4421-ab14-483cef3e297d',
    name: 'Right-sized next generation benchmark',
    description: 'Est sed et et ex ut aut. Eveniet nihil id voluptatum.',
    category: 'Events/General',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 99
  },
  {
    _id: 'ffda9143-fcab-4b46-92da-f6fa791b8bf5',
    name: 'Monitored heuristic parallelism',
    description:
      'Eos dolor rerum qui. Dolor qui dolores et in cumque praesentium qui repudiandae. Harum fuga ad omnis nihil quam sed voluptatibus ut cumque. Voluptas aspernatur natus maiores omnis qui et est ut et.',
    category: 'Events/General',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 61
  },
  {
    _id: '0a33eace-39a5-4b63-8cf3-6c9a7ee63706',
    name: 'Customer-focused real-time synergy',
    description:
      'Libero est et neque ut modi vitae. Laborum dolorem omnis quas error at. Et optio officia. Modi rerum dolorum id explicabo doloribus voluptatem ipsam. Sunt in omnis fugiat. Voluptatem explicabo quis esse.',
    category: 'Events/General',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 96
  },
  {
    _id: '7639b912-64cb-4635-8f88-89b407127539',
    name: 'Streamlined composite algorithm',
    description:
      'Repudiandae neque vero libero alias nemo voluptatem. Modi autem id explicabo dolore modi.',
    category: 'Events/General',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 93
  },
  {
    _id: '7c797a01-4c42-4e00-a20e-b84ec3d23f01',
    name: 'Down-sized optimal framework',
    description:
      'Id in dolores velit molestiae laudantium alias repellendus sapiente qui. Sunt doloremque et odio error corporis odio eveniet nemo sit. Sunt hic sequi debitis omnis voluptate soluta. Cumque incidunt atque eum blanditiis voluptate corporis aspernatur quidem. Minima modi dolorem reprehenderit praesentium sunt illo voluptas. Aliquid ea est dolores quia ut sed dolor commodi explicabo.',
    category: 'Events/General',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 57
  },
  {
    _id: 'a88f670b-8808-4978-9f8c-2463e67efdc4',
    name: 'Synergistic methodical framework',
    description:
      'In ut natus. Quos officia numquam iure et. Eligendi quidem tempora quae porro. Officia architecto enim ut ullam sed est quod nulla.',
    category: 'Events/General',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 73
  },
  {
    _id: 'c8503464-d4d2-44df-a992-5d8e872bfed7',
    name: 'Multi-channelled bi-directional moratorium',
    description:
      'Non unde quis quas quia assumenda perferendis eum. Tempora laboriosam sit omnis voluptatibus et commodi ea cumque autem. Sit in in ipsam eum labore dolores.',
    category: 'Events/General',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 73
  },
  {
    _id: 'a9933015-5eb9-489f-b45a-912e69501711',
    name: 'Configurable asymmetric policy',
    description:
      'Vero sed sed laborum est saepe atque repellat. Nostrum assumenda quia. Autem odio consequatur omnis beatae excepturi. Nesciunt earum officiis necessitatibus assumenda rerum deserunt.',
    category: 'Events/General',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 78
  },
  {
    _id: '61685a88-9b23-4f05-b22e-99b2f44180c5',
    name: 'Assimilated 3rd generation superstructure',
    description:
      'Aliquid dolores et velit non. Officia repellendus et molestias optio suscipit. Et qui iure mollitia quas porro ratione quam aliquam. Fugiat autem sed omnis officia quaerat. Est voluptatem maxime cupiditate vel pariatur. Et mollitia debitis nobis impedit commodi provident explicabo facilis eum.',
    category: 'Events/General',
    meta: '',
    booked_count: 50,
    booking_limit: 60,
    price: 81
  }
]

async function register({ name, college, email, mobile, userid, password }) {
  DATA.participant = {
    name,
    college,
    email,
    mobile,
    userid,
    password,
    short_id: 'T19EAC69F68E5F'
  }
  _saveToLocalStorage()
}

async function login({ userid, password }) {
  if (DATA.participant == null) {
    return false
  }

  return DATA.participant.userid === userid &&
    DATA.participant.password === password
    ? TOKEN
    : null
}

async function getEvents() {
  return _.clone(EVENTS)
}

async function registerEvent(token, event) {
  if (token !== TOKEN) return null
  DATA.participantRegisteredEvents.push(event._id)
  _saveToLocalStorage()
}

async function getParticipantProfile(token) {
  if (token !== TOKEN) return null
  const profile = _.pick(DATA.participant, ['name', 'college', 'short_id'])
  _.assign(profile, {
    registered_event_ids: DATA.participantRegisteredEvents,
    mac: 'TODO TODO TODO'
  })
  return profile
}

export default {
  register,
  login,
  getEvents,
  registerEvent,
  getParticipantProfile
}
