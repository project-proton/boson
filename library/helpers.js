import { ec as EC } from 'elliptic'
const ec = EC('secp256k1')
/**
 *
 * @param {string} category
 */
function getTopLevelCategory(category) {
  return category.split(':')[0]
}

/**
 *
 * @param {array} events Array of events
 */
function groupEventsByCategory(events) {
  const categories = {}
  for (const event of events) {
    const category = getTopLevelCategory(event.category || '')
    if (categories[category] === undefined) {
      categories[category] = []
    }
    categories[category].push(event)
  }
  return categories
}

function makeQrData(short_id, name, college, events, mac) {
  let data = ''
  const delimiter = '~'
  const event_delimiter = '#'
  const events_set = new Set(events)
  data += short_id
  data += delimiter
  data += name
  data += delimiter
  data += college
  data += delimiter
  for (const i in events_set) {
    data += i
    data += event_delimiter
  }
  data = data.slice(0, -1)
  return data
}

function verifyOfflineSignedQR(qrdata) {
  const data = Buffer.from(qrdata, 'base64').toString()
  const [pl, r, s] = data.split('.')
  console.log(pl)
  const [tathva_id, tathva_pass, bookings] = pl.split('-')
  const signature = { r, s }
  const pubstring =
    '0477a380ef44fe94d8006f8de7fa3b71089e21c5599555ba357927dd1f0b45124c76b5cb826234453b43d7b0351a9179a02fd9563beeac12b3bcb3a83bf1b1d878'
  const key = ec.keyFromPublic(pubstring, 'hex')
  if (key.verify(pl, signature)) {
    return {
      tathva_id,
      tathva_pass: Number(tathva_pass) === 1,
      bookings: Number(bookings)
    }
  } else return false
}

function landingPager() {
  return {
    data: 'data-admin',
    cert: 'cert-admin',
    mail: 'email-admin',
    reg: 'reg-admin',
    atnd: 'atnd-admin',
    pass: 'tathva-admin',
    scan: 'offline-qr'
  }
}

export default {
  makeQrData,
  groupEventsByCategory,
  verifyOfflineSignedQR,
  landingPager
}
